# BLOQUEADOR DE TELA e SCREENSAVER OLD-IS-COOL

Bloqueador de tela e screensaver (o segundo ainda necessita de correções) baseado
em programas simples implementados em BASIC, com geração de linhas coloridas em
posições aleatórias na tela do PC.

Sugere-se o uso do bloqueador com ativação de algum evento no WM, no caso de uso do
i3, pode-se usá-lo no lugar do programa i3lock. 

## Dependências

As únicas dependências para o uso do sistema são a biblioteca SDL2 e algum sistema 
operacional baseado em UNIX (não houveram testes com Windows, mas quem quiser arriscar
é bem vindo).

## Build

Para compilar os programas faz-se necessário o uso do programa make:

* Screensaver: `$ make`
* Screenlocker: `$ make block`
