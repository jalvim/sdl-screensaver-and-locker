// INSERÇÃO DE BIBLIOTECAS UTILIZADAS.
#include <stdlib.h>
#include <time.h>
#include <SDL2/SDL.h>

// DEFINIÇÃO DE MACROS USADAS PELO SISTEMA.
#define WIDTH 	1366
#define HEIGHT	 768
#define TRUE 	   1
#define FALSE	   0

// DEFINIÇÃO DE VARIÁVEIS GLOBAIS PARA FÁCIL ACESSO.
char quit = FALSE;
char type = 0;

void draw_circle(SDL_Renderer *render, int x, int y, int w, int h) {
	// FUNÇÃO DE DESENHO DE CÍRCULO COLORIDO NA JANELA
	// Determina raio de forma aleatória
	int radius = rand() % h / 10;

	// Itera por quadrado externo ao círculo
	for (int xx = 0; xx < 2 * radius; xx++)
		for (int yy = 0; yy < 2 * radius; yy++) {
			int dx = radius - xx;
			int dy = radius - yy;

			// Verifica se pixel pertence ao círculo e desenha ponto
			if (dx * dx + dy * dy < radius * radius &&
			    x + xx < w &&
			    y + yy < h)
				SDL_RenderDrawPoint(
					render,
					x + xx,
					y + yy
				);
		}
}

void drawLines(SDL_Window *window, SDL_Event event) {
	// FUNÇÃO DE DESENHO ALEATÓRIO DE LINHAS.
	SDL_Renderer *render = NULL; 			// Declara renderizador para linhas aleatórias.
	int rand_bw, rand_bh;				// Declara inteiros com condições de início aleatórias.
	int rand_ew, rand_eh;				// Declara inteiros com condições de fim aleatórias.
	int color[3];					// Declara cor aleatória.

	// Encontra tamanho total da janela
	int w, h;
	SDL_GetWindowSize(window, &w, &h);

	render = SDL_CreateRenderer(window, 		// Cria objeto render p/ manipulação da tela.
		-1, 
		SDL_RENDERER_ACCELERATED
	);
	SDL_SetRenderDrawColor(render, 			// Inicia renderização com cor escura.
		0, 0, 0, 
		SDL_ALPHA_OPAQUE
	);
	SDL_RenderClear(render);			// Limpa conteúdo presente no renderizador.
	quit = FALSE;					// Reinicia var. "quit" para estado FALSE.
	srand(time(NULL));				// Inicia semente para geração aleatória a partir do tempo. 

	SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);	// Reinicia eventos de pressão de tecla.

	while(!quit) {					// Cria Loop enquanto var. quit é falsa.
		rand_bh = rand() % h;			// Inicializa valores aleatórios para início e fim das linhas.
		rand_eh = rand() % h;			// Inicializa valores aleatórios para início e fim das linhas.
		rand_bw = rand() % w; 			// Inicializa valores aleatórios para início e fim das linhas.
		rand_ew = rand() % w; 			// Inicializa valores aleatórios para início e fim das linhas.
		for(int j=0; j<3; j++) {		// Itera por comprimento do vetor de cor.
			color[j] = rand() % 0xFF;	// Mantém espectro de cores dentro do limite 0xFF.
		}

		SDL_PollEvent(&event);			// Insere lista total de eventos em "event".

		switch(event.type) {			// Verifica qual tipo de evento está na var. "event".
		case SDL_KEYDOWN:			// Verifica se houve evento KEY_DOWN.
		case SDL_KEYUP:				// Verifica se houve evento KEY_UP.
		case SDL_QUIT:				// Verifica se houve evento QUIT.
			quit = TRUE;			// Faz var. "quit" verdadeira.
			break;				// Encerra switch.
		case SDL_MOUSEMOTION:			// Verifica evento de mov. do mouse.
			SDL_SetRenderDrawColor(		// Inicia renderização sobre cor preta.
				render, 
				0, 0, 0, 
				SDL_ALPHA_OPAQUE
			);
			SDL_RenderClear(render);	// Caso positivo, reinicia tela de renderização.
			SDL_FlushEvent(SDL_MOUSEMOTION);
			break;
		default:
			// CRIA OBJETO DE TEXTURA PARA RENDERIZAÇÃO DAS LINHAS.
			SDL_SetRenderDrawColor(
				render, 
				color[0], 
				color[1], 
				color[2], 
				SDL_ALPHA_OPAQUE
			);

			if (type == 0) {
				SDL_RenderDrawLine(
					render, 
					rand_bw, 
					rand_bh, 
					rand_ew, 
					rand_eh
				);
			} else {
				draw_circle(
					render, 
					rand_bw, 
					rand_bh, 
					w,
					h
				);
			}

			SDL_RenderPresent(render);	// Carrega renderização na janela criada.

			SDL_Delay(500);			// Gera delay de 500 ms.
			break;				// Encerra ciclo de "switch".
		}
	}

	SDL_RenderClear(render);			// Libera a memória do programa.
	SDL_DestroyRenderer(render); 			// Libera a memória do programa.
}

int main(int argc, char* args[]) {
	// Verifica se foi dado input -> impressão de bola
	if (argc > 1)
		type = 1;

	SDL_Window *window = NULL;			// Cria janela SDL ainda não inicializada.
	SDL_Event   event;				// Cria estrutura de eventos interna.

	SDL_Delay(500);					// Espera 1 segundo -> Liberar os eventos
	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(
			stderr, 
			"Nao foi possivel inicializar SDL: %s\n", 
			SDL_GetError()
		);
		return 1;				// Encerra programa com erro.
	}

	SDL_FlushEvents(				// Reinicia todos os eventos presentes na fila.
		SDL_FIRSTEVENT, 			// Flag de Primeiro evento.
		SDL_LASTEVENT				// Flag de Último evento.
	);

	window = SDL_CreateWindow(
		"Hello World!",				// Cria janela SDL2.
		SDL_WINDOWPOS_UNDEFINED,         	// Cria janela SDL2.
		SDL_WINDOWPOS_UNDEFINED,         	// Cria janela SDL2.
		WIDTH,                           	// Cria janela SDL2.
		HEIGHT,                          	// Cria janela SDL2.
		SDL_WINDOW_SHOWN | 			// Flag de janela exposta.
		SDL_WINDOW_FULLSCREEN_DESKTOP 		// Flag de modo tela cheia.
	);

	if(window == NULL) {				// Caso a janela não seja inicializada,
		goto END;				// encerra aplicação com erro de inicialização de janela.
	}

	drawLines(window, event);			// Chama a função de pintura de linhas aleatórias.
	SDL_DestroyWindow(window); 			// Libera a memória do programa.
	SDL_Quit();                			// Libera a memória do programa.

	return 0;					// Encerra a função principal.

END:
	fprintf(
		stderr, 
		"Janela n pode ser criada: %s\n", 
		SDL_GetError()
	);
	return 1;					// Encerra programa com erro.
}
