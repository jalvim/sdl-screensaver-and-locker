# Arquivo para compilação do prjeto de teste da biblioteca
# SDL2 --> Ainda é necessário integração com biblioteca X11
# para funcionalidade de protetor de tela.

# Declaração de comiplador utilizado e flags de compilação.
CC = gcc
CFLAGS = -g -Wall

# Declaração de flags de bibliotecas necessárias para a compilação do arquivo.
LIBFLAGS = `sdl2-config --cflags --libs`
TARGET = SDL_teste
BLOCK = SDL_block

all: $(TARGET) $(BLOCK)

teste: $(TARGET)

$(TARGET): $(TARGET).c
	$(CC) $(CFLAGS) -o $(TARGET) $(TARGET).c $(LIBFLAGS)

block: $(BLOCK) 

$(BLOCK): $(BLOCK).c
	$(CC) $(CFLAGS) -o $(BLOCK) $(BLOCK).c $(LIBFLAGS)
