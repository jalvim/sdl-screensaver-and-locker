// INSERÇÃO DE BIBLIOTECAS UTILIZADAS.
#include <stdlib.h>
#include <time.h>
#include <SDL2/SDL.h>

// DEFINIÇÃO DE MACROS USADAS PELO SISTEMA.
#define WIDTH 	1366
#define HEIGHT	 768
#define TRUE 	   1
#define FALSE	   0

// DEFINIÇÃO DE VARIÁVEIS GLOBAIS PARA FÁCIL ACESSO.
char i = 0;
int red[]   = {0xFF, 0x00, 0x00};
int green[] = {0x00, 0xFF, 0x00};
int blue[]  = {0x00, 0x00, 0xFF};
char quit = FALSE;

// ALÉM DE CRIAR LOOP QUE ATIVA A JANELA A CADA 120 SEGUNDOS.

void colorSurf(SDL_Window *window, SDL_Surface *screenSur, SDL_Event event) {
	screenSur = SDL_GetWindowSurface(window);	// Cria estrutura de superfície sobre a janela "window".

	SDL_FlushEvent(SDL_KEYUP);			// Reinicia eventos de pressão de tecla.
	SDL_FlushEvent(SDL_KEYDOWN);			// Reinicia eventos de pressão de tecla.
	SDL_FlushEvent(SDL_QUIT);			// Reinicia eventos de encerramento.

	while(!quit) {					// Cria Loop enquanto var. quit é falsa.
		SDL_PollEvent(&event);			// Insere lista total de eventos em "event".
		switch(event.type) {			// Verifica qual tipo de evento está na var. "event".
		case SDL_KEYDOWN:			// Verifica se houve evento KEY_DOWN.
		case SDL_KEYUP:				// Verifica se houve evento KEY_UP.
		case SDL_QUIT:				// Verifica se houve evento QUIT.
			quit = TRUE;			// Faz var. "quit" verdadeira.
			break;				// Encerra switch.
		default:				// Caso nenhum dos eventos de interesse seja detectado, executa:
			SDL_FillRect(
				screenSur,
			       	NULL,
				SDL_MapRGB(
					screenSur->format, 
					red[i % 3], 
					green[i % 3], 
					blue[i % 3]
				)
			);

			SDL_UpdateWindowSurface(window);
			SDL_Delay(1000);
			i++;
			i = i < 8 ? i : 0;
			break;
		}
	}
}

void drawLines(SDL_Window *window, SDL_Event event) {
	// FUNÇÃO DE DESENHO ALEATÓRIO DE LINHAS.
	SDL_Renderer *render = NULL; 			// Declara renderizador para linhas aleatórias.
	int rand_bw, rand_bh;				// Declara inteiros com condições de início aleatórias.
	int rand_ew, rand_eh;				// Declara inteiros com condições de fim aleatórias.

	render = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawColor(render, 0, 0, 0, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(render);
	quit = FALSE;					// Reinicia var. "quit" para estado FALSE.
	srand(time(NULL));				// Inicia semente para geração aleatória a partir do tempo. 

	SDL_FlushEvent(SDL_KEYUP);			// Reinicia eventos de pressão de tecla.
	SDL_FlushEvent(SDL_KEYDOWN);			// Reinicia eventos de pressão de tecla.
	SDL_FlushEvent(SDL_QUIT);			// Reinicia eventos de encerramento.

	while(!quit) {					// Cria Loop enquanto var. quit é falsa.
		rand_bh = rand() % HEIGHT;		// Inicializa valores aleatórios para início e fim das linhas.
		rand_eh = rand() % HEIGHT;		// Inicializa valores aleatórios para início e fim das linhas.		
		rand_bw = rand() % WIDTH; 		// Inicializa valores aleatórios para início e fim das linhas.		
		rand_ew = rand() % WIDTH; 		// Inicializa valores aleatórios para início e fim das linhas.		

		SDL_PollEvent(&event);			// Insere lista total de eventos em "event".

		switch(event.type) {			// Verifica qual tipo de evento está na var. "event".
		case SDL_KEYDOWN:			// Verifica se houve evento KEY_DOWN.
		case SDL_KEYUP:				// Verifica se houve evento KEY_UP.
		case SDL_QUIT:				// Verifica se houve evento QUIT.
			quit = TRUE;			// Faz var. "quit" verdadeira.
			break;				// Encerra switch.
		case SDL_MOUSEMOTION:			// Verifica evento de mov. do mouse.
			SDL_SetRenderDrawColor(
				render, 
				0, 0, 0, 
				SDL_ALPHA_OPAQUE
			);
			SDL_RenderClear(render);	// Caso positivo, reinicia tela de renderização.
			SDL_FlushEvent(SDL_MOUSEMOTION);
			break;
		default:
			// CRIA OBJETO DE TEXTURA PARA RENDERIZAÇÃO DAS LINHAS.
			SDL_SetRenderDrawColor(
				render, 
				red[i % 3], 
				green[i % 3], 
				blue[i % 3], 
				SDL_ALPHA_OPAQUE
			);
			SDL_RenderDrawLine(
				render, 
				rand_bw, 
				rand_bh, 
				rand_ew, 
				rand_eh
			);
			SDL_RenderPresent(render);	// Carrega renderização na janela criada.

			SDL_Delay(500);			// Gera delay de 500 ms.
			i++;				// Incrementa índice "i".
			break;				// Encerra ciclo de "switch".
		}
	}

	SDL_RenderClear(render);			// Libera a memória do programa.
	SDL_DestroyRenderer(render); 			// Libera a memória do programa.
}

int main(int argc, char* args[]) {
	SDL_Window *window = NULL;			// Cria janela SDL ainda não inicializada.
	SDL_Window *GlobalWindow = NULL;		// Cria janela Global invisível para loop de eventos principal.
	//SDL_Surface *screenSur = NULL;
	SDL_Event event;				// Cria estrutura de eventos interna.
	SDL_Event globalEv;				// Cria estrutura de eventos global.
	clock_t time1, time2;				// Cria estruturas de tempo p/ análise diferencial.
	double time_tot;				// Cria número p/ cálculo de tempo total.
	char flag = FALSE;				// Flag de contagem de tempo ocioso.

	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "Nao foi possivel inicializar SDL: %s\n", SDL_GetError());
		return 1;				// Encerra programa com erro.
	}

	GlobalWindow = SDL_CreateWindow("Global",	// Cria janela SDL2.
		SDL_WINDOWPOS_UNDEFINED,         	// Cria janela SDL2.
		SDL_WINDOWPOS_UNDEFINED,         	// Cria janela SDL2.
		WIDTH,                           	// Cria janela SDL2.
		HEIGHT,                          	// Cria janela SDL2.
		SDL_WINDOW_HIDDEN | 			// Flag de janela escondida.
		SDL_WINDOW_FULLSCREEN_DESKTOP		// Flag de modo tela cheia.
	);

	if(GlobalWindow == NULL) {
		goto END;
	}

	while(TRUE) {					// Inicia loop principal de operação do programa.

		SDL_FlushEvents(			// Reinicia todos os eventos presentes na fila.
			SDL_FIRSTEVENT, 		// Flag de Primeiro evento.
			SDL_LASTEVENT			// Flag de Último evento.
		);

		time1 = clock();			// Inicia contagem de tempo sem evento.
		while((SDL_PollEvent(&globalEv) == 0)) {	// Verifica se ainda não há nenhum evento global.
			time2 = clock();			// Caso sim, adiciona contagem em time2.
			time_tot = ((double) time2 - time1) / CLOCKS_PER_SEC;	// Determina tempo total pela diferença de time1 e time2
			if(time_tot >= 10) {					// Se time_tot for superior a 3 min,
				flag = TRUE;					// Faz flag verdadeira.
				break;						// Encerra laço de falta de eventos.
			}

		}

		if(flag) {				// Verifica se a flag é positiva.
			window = SDL_CreateWindow("Hello World!",	// Cria janela SDL2.
				SDL_WINDOWPOS_UNDEFINED,         	// Cria janela SDL2.
				SDL_WINDOWPOS_UNDEFINED,         	// Cria janela SDL2.
				WIDTH,                           	// Cria janela SDL2.
				HEIGHT,                          	// Cria janela SDL2.
				SDL_WINDOW_SHOWN | 			// Flag de janela exposta.
				SDL_WINDOW_FULLSCREEN_DESKTOP 		// Flag de modo tela cheia.
			);

			if(window == NULL) {		// Caso a janela não seja inicializada,
				goto END;		// encerra aplicação com erro de inicialização de janela.
			}

			drawLines(window, event);	// Chama a função de pintura de linhas aleatórias.
			SDL_DestroyWindow(window); 	// Libera a memória do programa.
			window = NULL;			// Reinicia janela.
			flag = FALSE;			// Reseta flag de display.
		}

		// CICLO DE FINALIZAÇÃO DO LAÇO PRINCIPAL.
		//XNextEvent(disp, &inEvent);

		//if(inEvent.type == KeyPress || inEvent.type == KeyRelease) {
		//	if(inEvent.xkey.keycode == XK_Escape) 
		//		break;
		//}
	}

	//SDL_FreeSurface(screenSur);			// Libera a memória do programa.
	SDL_DestroyWindow(GlobalWindow); 		// Libera a memória do programa.
	SDL_Quit();                			// Libera a memória do programa.

	return 0;					// Encerra a função principal.

END:
	fprintf(stderr, "Janela n pode ser criada: %s\n", SDL_GetError());
	return 1;				// Encerra programa com erro.
}
